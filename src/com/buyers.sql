
create table buyers
(
    id bigserial primary key,
    namebuyers varchar(30) not null,
    number varchar(30) unique
);

insert into buyers(namebuyers, number)
values ('Илья', '8-999-999-88-99');
insert into buyers(namebuyers, number)
values ('Андрей', '8-888-999-88-99');
insert into buyers(namebuyers, number)
values ('Сергей', '8-777-999-88-99');

SELECT *
from buyers;
commit;


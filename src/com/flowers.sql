
create table flowers
(
    id serial primary key,
    flower varchar(15) not null,
    price integer not null
);

insert into flowers(flower, price)
values ('Розы', 100);
insert into flowers(flower, price)
values ('Лилии', 50);
insert into flowers(flower, price)
values ('Ромашки', 25);


SELECT *
from flowers;
commit;






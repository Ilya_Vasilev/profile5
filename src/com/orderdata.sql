
create table orderdata
(
    id bigserial primary key,
    buyer_id integer references buyers(id),
    flower_id integer references flowers(id),
    quantity integer check (quantity >= 1 and quantity <=1000),
    datatime timestamp not null
);

insert into orderdata (buyer_id,flower_id,quantity,datatime) values(2, 1, 100, now() - interval '24 days');
insert into orderdata (buyer_id,flower_id,quantity, datatime) values(1, 2, 35,now() - interval '12 days');
insert into orderdata (buyer_id,flower_id,quantity, datatime) values(3, 2, 50,now() - interval '4 days');
insert into orderdata (buyer_id,flower_id,quantity, datatime) values(3, 1, 25,now() - interval '2 days');
insert into orderdata (buyer_id,flower_id,quantity, datatime) values(1, 2, 13,now());


SELECT *
from orderdata;
commit;


-- По идентификатору заказа получить данные заказа и данные клиента,создавшего этот заказ
select * from orderdata
join buyers on orderdata.buyer_id = buyers.id and orderdata.id = 3;

--Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
select * from orderdata
where buyer_id = 1 and datatime > now() - interval '1 month'

--Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
select f.flower, o.quantity
from flowers f
join orderdata o on f.id = o.flower_id
where o.quantity = (select max(quantity) from orderdata);

--Вывести общую выручку (сумму золотых монет по всем заказам) за все время
select sum(o.quantity * f.price)
from orderdata o, flowers f
where o.flower_id = f.id;
